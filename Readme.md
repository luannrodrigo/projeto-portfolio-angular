# Projeto Crud-Portfólio
    O projeto foi criado para cadastrar e gerenciar todos os projeto desenvolvidos.
#   Tecnologias utilizadas
        Para o Front-End foi utilizado Angular como framework para a estilização das paginas. 
        Para o Back-End foi utilizado o MongoDb, juntamente com o Mongoose como base e gerenciamento de dados, node.js para poder utilizar o js no servidor. 

# Requitos
    Node.js versão v8.11.4 ou superior
    MongoDB versão v4.0.4
    Angular versão v6.1.10

#   Para instalação do back-end
#   1º Baixe o projeto usando o git
        git clone https://luannrodrigo@bitbucket.org/luannrodrigo/projeto-portfolio-angular.git
#   2º Navegue até o diretório back-end
        cd /path/to/back-end
#   3º Instale os modulos
        npm i
#   4º Start o back-end
        npm start

#   Para instalação do Front-End
#   1º Navegue até o diretório front-end
        cd /path/to/front-end
#   2º Instale os modulos
        npm i
#   3º Start o front-end
        ng serve
#   4º Acesse no navegador o endereço http://localhost:4200/

