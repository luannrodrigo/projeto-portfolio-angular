var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const cors = require('cors');
app.use(cors());

const db = require('./config/database');

db('mongodb://localhost:27017/portifolio');

//liganco aplicação a rota e associando
const portifolio = require('./routes/portifolio');
app.use('/portifolio', portifolio);

const texto = require('./routes/texto');
app.use('/texto', texto);

const skill = require('./routes/skill');
app.use('/skill', skill);

const users = require('./routes/users');
app.use('/users', users);

const contato = require('./routes/contato')
app.use('/contato', contato)
console.log('api rodando')

module.exports = app;
