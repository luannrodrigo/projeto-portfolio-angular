const mongoose = require('mongoose');

module.exports = function(){
    const schema = mongoose.Schema({
        usuario:{
            type: String,
            required: true
        },
        senha:{
            type: Number,
            required: true
        }
    });
    return mongoose.model('Users', schema, 'users');
};
