const mongoose = require('mongoose');
module.exports = function () {
    const schema = mongoose.Schema({
        nome_skill: {
            type: String,
            required: true
        },
        desc_skill:{
            type: String,
            required: true
        },
        usuario:{
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Users',
            required: true
        }
    });
    return mongoose.model('Skill', schema, 'skills');
}
