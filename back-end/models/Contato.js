const mongoose = require('mongoose');


module.exports = function(){
    const schema = mongoose.Schema({
        nome_contato:{
            type: String,
            required: true
        },
        email_contato:{
            type: String,
            required: true
        },
        mensagem:{
            type: String,
            required: true
        }
    });
    return mongoose.model('Contato', schema, 'contatos');
};
