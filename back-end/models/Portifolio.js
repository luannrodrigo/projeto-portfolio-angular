const mongoose = require('mongoose');


module.exports = function(){
    const schema = mongoose.Schema({
        nome_projeto:{
            type: String,
            required: true
        },
        usuario:{
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Users',
            required: true
        }
    });
    return mongoose.model('Portifolio', schema, 'portifolios');
};
