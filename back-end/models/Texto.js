const mongoose = require('mongoose');

module.exports = function(){
    const schema = mongoose.Schema({
        titulo:{
            type: String,
            required: true
        },
        texto:{
            type: String,
            required: true
        },
        usuario:{
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'Users',
            required: true
        }
    });
    return mongoose.model('Texto', schema, 'textos');
};
