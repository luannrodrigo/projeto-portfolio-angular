const Contato = require('../models/Contato')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Contato.create(req.body).then(
            // Callback caso a requisição seja certa
            function () {
                res.send(null);
                res.send(201).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function (req, res) {
        Contato.find().exec().then(
            // ok
            function (contatos) {
                res.json(contatos).end();
            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function (req, res) {
        Contato.findById(req.params.id).exec().then(
            function (contatos) {
                if (contatos) {
                    res.json(contatos);
                } else {
                    res.status(404).end();
                }

            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function (req, res) {
        Contato.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function () {
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function (req, res) {
        Contato.findByIdAndRemove(req.params.id).exec().then(
            function () {
                res.send(`Elemento excluido com sucesso ${204}`).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
