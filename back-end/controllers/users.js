const Users = require('../models/Users')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Users.create(req.body).then(
            // Callback caso a requisição seja certa
            function (users) {
                res.send(null);
                res.send(201).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function (req, res) {
        Users.find().exec().then(
            // ok
            function (Users) {
                res.json(Users).end();
            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function (req, res) {
        Users.findById(req.params.id).exec().then(
            function (Users) {
                if (Users) {
                    res.json(Users);
                } else {
                    res.status(404).end();
                }

            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function (req, res) {
        Users.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function () {
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function (req, res) {
        Users.findByIdAndRemove(req.params.id).exec().then(
            function () {
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
