const Texto = require('../models/Texto')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Texto.create(req.body).then(
            // Callback caso a requisição seja certa
            function () {
                // HTTP 201: Criado
                res.send(null);
                res.sendStatus(201).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function (req, res) {
        Texto.find().populate('usuario').exec().then(
            // ok
            function (textos) {
                res.json(textos).end();
            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function (req, res) {
        Texto.findById(req.params.id).exec().then(
            function (textos) {
                if (textos) {
                    res.json(textos);
                } else {
                    res.status(404).end();
                }

            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function (req, res) {
        Texto.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function () {
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function (req, res) {
        Texto.findByIdAndRemove(req.params.id).exec().then(
            function () {
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
