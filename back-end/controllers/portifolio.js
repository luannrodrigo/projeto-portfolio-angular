const Portifolio = require('../models/Portifolio')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Portifolio.create(req.body).then(
            // Callback caso a requisição seja certa
            function () {
                res.send(null);
                res.sendStatus(201).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function (req, res) {
        Portifolio.find().populate('usuario').exec().then(
            // ok
            function (portifolios) {
                res.json(portifolios).end();
            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function (req, res) {
        Portifolio.findById(req.params.id).exec().then(
            function (portifolios) {
                if (portifolios) {
                    res.json(portifolios);
                } else {
                    res.status(404).end();
                }

            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.atualizar = function (req, res) {
        Portifolio.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function () {
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function (req, res) {
        Portifolio.findByIdAndRemove(req.params.id).exec().then(
            function () {
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
