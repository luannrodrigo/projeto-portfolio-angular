const Skill = require('../models/Skill')(/*contrutor*/);

module.exports = function () {
    const controller = {};

    controller.novo = function (req, res) {
        // criando um novo documento no banco
        Skill.create(req.body).then(
            // Callback caso a requisição seja certa
            function () {
                res.send(null);
                res.send(201).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    controller.listar = function (req, res) {
        Skill.find().populate('usuario').exec().then(
            // ok
            function (skills) {
                res.json(skills).end();
            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }
    controller.obterUm = function (req, res) {
        Skill.findById(req.params.id).exec().then(
            function (skills) {
                if (skillss) {
                    res.json(skills);
                } else {
                    res.status(404).end();
                }

            },
            function (erro) {
                console.log(erro);
                res.status(500).end();
            }
        );
    }

    controller.atualizar = function (req, res) {

        Artigo.findByIdAndUpdate(req.body._id, req.body).exec().then(
            function () {
                // HTTP 204: OK, sem dados
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );

    }

    controller.excluir = function (req, res) {
        Skill.findByIdAndRemove(req.params.id).exec().then(
            function () {
                res.send(204).end();
            },
            function (erro) {
                console.log(erro);
                res.send(500).end();
            }
        );
    }
    return controller;
};
