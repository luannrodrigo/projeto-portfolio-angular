import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { TextoListComponent } from './portfolio/texto-list/texto-list.component';
import { TextoFormComponent } from './portfolio/texto-form/texto-form.component';

import { UsersListComponent } from './portfolio/users-list/users-list.component';
import { UsersFormComponent } from './portfolio/users-form/users-form.component';

import { SkillListComponent } from './portfolio/skill-list/skill-list.component';
import { SkillFormComponent } from './portfolio/skill-form/skill-form.component';

import { PortifolioListComponent } from './portfolio/portifolio-list/portifolio-list.component';
import { PortifolioFormComponent } from './portfolio/portifolio-form/portifolio-form.component';

import { ContatoListComponent } from './portfolio/contato-list/contato-list.component';
import { ContatoFormComponent } from './portfolio/contato-form/contato-form.component';

const routes: Routes = [
  { path: 'texto', component: TextoListComponent },
  { path: 'texto/novo', component: TextoFormComponent },
  { path: 'texto/:id', component: TextoFormComponent },

  { path: 'users', component: UsersListComponent },
  { path: 'users/novo', component: UsersFormComponent },
  { path: 'users/:id', component: UsersFormComponent },
  
  { path: 'skill', component: SkillListComponent },
  { path: 'skill/novo', component: SkillFormComponent },
  { path: 'skill/:id', component: SkillFormComponent },

  { path: 'portifolio', component: PortifolioListComponent },
  { path: 'portifolio/novo', component: PortifolioFormComponent },
  { path: 'portifolio/:id', component: PortifolioFormComponent },

  { path: 'contato', component: ContatoListComponent },
  { path: 'contato/novo', component: ContatoFormComponent },
  { path: 'contato/:id', component: ContatoFormComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
