import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PortifolioService {

  // Injeção de dependência nos parâmetros do construtor
  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get('http://localhost:3000/portifolio');
  }

  obterUm(id: String) {
    return this.http.get('http://localhost:3000/portifolio/' + id);
  }

  novo(portifolio) {
    return this.http.put('http://localhost:3000/portifolio', portifolio);
  }

  atualizar(portifolio) {
    return this.http.patch('http://localhost:3000/portifolio', portifolio);
  }

  excluir(id: String) {
    return this.http.delete('http://localhost:3000/portifolio/' + id);
  }

}
