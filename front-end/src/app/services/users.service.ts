import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  // Injeção de dependência nos parâmetros do construtor
  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get('http://localhost:3000/users');
  }

  obterUm(id: String) {
    return this.http.get('http://localhost:3000/users/' + id);
  }

  novo(users) {
    return this.http.put('http://localhost:3000/users', users);
  }

  atualizar(users) {
    return this.http.patch('http://localhost:3000/users', users);
  }

  excluir(id: String) {
    return this.http.delete('http://localhost:3000/users/' + id);
  }

}
