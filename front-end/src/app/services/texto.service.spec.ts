import { TestBed } from '@angular/core/testing';

import { TextoService } from './texto.service';

describe('TextoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TextoService = TestBed.get(TextoService);
    expect(service).toBeTruthy();
  });
});
