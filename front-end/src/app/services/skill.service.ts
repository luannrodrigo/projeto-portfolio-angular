import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  // Injeção de dependência nos parâmetros do construtor
  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get('http://localhost:3000/skill');
  }

  obterUm(id: String) {
    return this.http.get('http://localhost:3000/skill/' + id);
  }

  novo(skill) {
    return this.http.put('http://localhost:3000/skill', skill);
  }

  atualizar(skill) {
    return this.http.patch('http://localhost:3000/skill', skill);
  }

  excluir(id: String) {
    return this.http.delete('http://localhost:3000/skill/' + id);
  }

}
