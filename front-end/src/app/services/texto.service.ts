import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TextoService {

  // Injeção de dependência nos parâmetros do construtor
  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get('http://localhost:3000/texto');
  }

  obterUm(id: String) {
    return this.http.get('http://localhost:3000/texto/' + id);
  }

  novo(texto) {
    return this.http.put('http://localhost:3000/texto', texto);
  }

  atualizar(texto) {
    return this.http.patch('http://localhost:3000/texto', texto);
  }

  excluir(id: String) {
    return this.http.delete('http://localhost:3000/texto/' + id);
  }

}
