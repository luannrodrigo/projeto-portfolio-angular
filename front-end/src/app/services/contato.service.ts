import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContatoService {

  // Injeção de dependência nos parâmetros do construtor
  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get('http://localhost:3000/contato');
  }

  obterUm(id: String) {
    return this.http.get('http://localhost:3000/contato/' + id);
  }

  novo(contato) {
    return this.http.put('http://localhost:3000/contato', contato);
  }

  atualizar(contato) {
    return this.http.patch('http://localhost:3000/contato', contato);
  }

  excluir(id: String) {
    return this.http.delete('http://localhost:3000/contato/' + id);
  }

}
