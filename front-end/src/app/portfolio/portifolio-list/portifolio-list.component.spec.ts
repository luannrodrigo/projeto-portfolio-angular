import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortifolioListComponent } from './portifolio-list.component';

describe('PortifolioListComponent', () => {
  let component: PortifolioListComponent;
  let fixture: ComponentFixture<PortifolioListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortifolioListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortifolioListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
