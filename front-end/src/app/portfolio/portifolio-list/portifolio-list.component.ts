import { Component, OnInit } from '@angular/core';
import { PortifolioService } from '../../services/portifolio.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-portifolio-list',
  templateUrl: './portifolio-list.component.html',
  styleUrls: ['./portifolio-list.component.css']
})
export class PortifolioListComponent implements OnInit {

  public portifolios: any;

  public colunasVisiveis: string[] = [
    'nome_projeto',
    'usuario', 
    'excluir'
  ];

  // Injeção de dependência no parâmetro do construtor
  constructor(
    private portifolioSrv: PortifolioService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    // Chamando o service
    this.portifolioSrv.listar().subscribe(
       dados => this.portifolios = dados, // Callback do bem
       erro => console.error(erro) // Callback do mal
    );
  }

  excluir(id: String) {
    if (confirm('Deseja realmente excluir este artigo?')) {
      this.portifolioSrv.excluir(id).subscribe(
        () => {
          this.snackBar.open('Artigo excluído com sucesso', 'OK', { duration: 2000});
          this.ngOnInit(); // Recarrega a lista
        },
        erro => this.snackBar.open('ERRO AO EXCLUIR ARTIGO: ' + erro.message, 'OK')
      );
    }
  }

}
