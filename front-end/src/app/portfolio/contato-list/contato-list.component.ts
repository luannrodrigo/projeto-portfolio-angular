import { Component, OnInit } from '@angular/core';
import { ContatoService } from '../../services/contato.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-contato-list',
  templateUrl: './contato-list.component.html',
  styleUrls: ['./contato-list.component.css']
})
export class ContatoListComponent implements OnInit {

  public contatos: any;

  public colunasVisiveis: string[] = [
    'nome_contato',
    'email_contato',
    'mensagem', 
    'excluir'
  ];

  // Injeção de dependência no parâmetro do construtor
  constructor(
    private contatoSrv: ContatoService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    // Chamando o service
    this.contatoSrv.listar().subscribe(
       dados => this.contatos = dados, // Callback do bem
       erro => console.error(erro) // Callback do mal
    );
  }

  excluir(id: String) {
    if (confirm('Deseja realmente excluir este artigo?')) {
      this.contatoSrv.excluir(id).subscribe(
        () => {
          this.snackBar.open('Artigo excluído com sucesso', 'OK', { duration: 2000});
          this.ngOnInit(); // Recarrega a lista
        },
        erro => this.snackBar.open('ERRO AO EXCLUIR ARTIGO: ' + erro.message, 'OK')
      );
    }
  }

}
