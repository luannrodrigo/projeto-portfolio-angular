import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PortifolioFormComponent } from './portifolio-form.component';

describe('PortifolioFormComponent', () => {
  let component: PortifolioFormComponent;
  let fixture: ComponentFixture<PortifolioFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PortifolioFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortifolioFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
