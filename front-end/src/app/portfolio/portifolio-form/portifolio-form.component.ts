import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PortifolioService } from '../../services/portifolio.service';
import { UsersService } from '../../services/users.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-portifolio-form',
  templateUrl: './portifolio-form.component.html',
  styleUrls: ['./portifolio-form.component.css']
})
export class PortifolioFormComponent implements OnInit {

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private portifolioSrv: PortifolioService,
    private usersSrv: UsersService, 
    private snackBar: MatSnackBar
  ) { }

  public titulo: String = 'Novo Portfolio';

  public portifolio: any = {conjunto: false};
  public usuarios: any = [];


  ngOnInit() {

    // Pedimos para que o ActivatedRoute busque para nós os parâmetros da url
    this.actRoute.params.subscribe(  // Chamada assíncrona
      params => {
        if (params.id) { // Se existir um parâmetro chamado id
          // 1) Vamos buscar os dados do objeto no back-end
          this.portifolioSrv.obterUm(params.id).subscribe(
            obj => { // Callback se OK
              this.portifolio = obj;
              console.log(obj);
              this.titulo = 'Editar portifolio';
            },
            erro => console.error(erro) // Callback se erro
          );
        }
      }
    );

    // this.portifolioSrv.listar().subscribe(
    //   dados => {
    //     this.portifolio = dados;
    //     console.log(dados);
    //   },
    //   erro => console.error(erro)
    // );


    this.usersSrv.listar().subscribe(
      dados => {
        this.usuarios = dados;
        console.log(dados);
      },
      erro => console.error(erro)
    );
  }

  salvar() {
    let retorno: any;
    if (this.portifolio._id) {
      retorno = this.portifolioSrv.atualizar(this.portifolio);
    } else {
      retorno = this.portifolioSrv.novo(this.portifolio);
    }
    retorno.subscribe(
      () => {
        this.snackBar.open('Portifolio salvo com sucesso', 'OK', { duration: 2000 });
        this.router.navigate(['portifolio']); // Volta à listagem
      },
      erro => {
        this.snackBar.open('Erro ao salvar o portifolio: ' + erro.message, 'OK');
        console.error(erro);
      }
    );
  }

  cancelar() {
    if (confirm('Deseja realmente cancelar as alterações?')) {
      this.router.navigate(['portifolio']);
    }
  }


}
