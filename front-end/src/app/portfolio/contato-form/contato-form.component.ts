import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ContatoService } from '../../services/contato.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-contato-form',
  templateUrl: './contato-form.component.html',
  styleUrls: ['./contato-form.component.css']
})
export class ContatoFormComponent implements OnInit {

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private contatoSrv: ContatoService,
    private snackBar: MatSnackBar
  ) { }

  public titulo: String = 'Contato';

  public contato: any = {conjunto: false};


  ngOnInit() {

    // Pedimos para que o ActivatedRoute busque para nós os parâmetros da url
    this.actRoute.params.subscribe(  // Chamada assíncrona
      params => {
        if (params.id) { // Se existir um parâmetro chamado id
          // 1) Vamos buscar os dados do objeto no back-end
          this.contatoSrv.obterUm(params.id).subscribe(
            obj => { // Callback se OK
              this.contato = obj;
              console.log(obj);
              this.titulo = 'Editar contato';
            },
            erro => console.error(erro) // Callback se erro
          );
        }
      }
    );

    // this.contatoSrv.listar().subscribe(
    //   dados => {
    //     this.contato = dados;
    //     console.log(dados);
    //   },
    //   erro => console.error(erro)
    // );
  }

  salvar() {
    let retorno: any;
    if (this.contato._id) {
      retorno = this.contatoSrv.atualizar(this.contato);
    } else {
      retorno = this.contatoSrv.novo(this.contato);
    }
    retorno.subscribe(
      () => {
        this.snackBar.open('Contato salvo com sucesso', 'OK', { duration: 2000 });
        this.router.navigate(['contato']); // Volta à listagem
      },
      erro => {
        this.snackBar.open('Erro ao salvar o contato: ' + erro.message, 'OK');
        console.error(erro);
      }
    );
  }

  cancelar() {
    if (confirm('Deseja realmente cancelar as alterações?')) {
      this.router.navigate(['contato']);
    }
  }


}
