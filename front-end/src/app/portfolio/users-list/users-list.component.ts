import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  public userss: any;

  public colunasVisiveis: string[] = [
    'usuario', 
    'senha',
    'excluir'
  ];

  // Injeção de dependência no parâmetro do construtor
  constructor(
    private usersSrv: UsersService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    // Chamando o service
    this.usersSrv.listar().subscribe(
       dados => this.userss = dados, // Callback do bem
       erro => console.error(erro) // Callback do mal
    );
  }

  excluir(id: String) {
    if (confirm('Deseja realmente excluir este artigo?')) {
      this.usersSrv.excluir(id).subscribe(
        () => {
          this.snackBar.open('Artigo excluído com sucesso', 'OK', { duration: 2000});
          this.ngOnInit(); // Recarrega a lista
        },
        erro => this.snackBar.open('ERRO AO EXCLUIR ARTIGO: ' + erro.message, 'OK')
      );
    }
  }

}
