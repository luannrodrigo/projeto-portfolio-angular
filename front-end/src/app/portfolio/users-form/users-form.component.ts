import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private usersSrv: UsersService,
    private snackBar: MatSnackBar
  ) { }

  public titulo: String = 'Novo Usuário';

  public users: any = {conjunto: false};
  // public usuarios: any = [];


  ngOnInit() {

    // Pedimos para que o ActivatedRoute busque para nós os parâmetros da url
    this.actRoute.params.subscribe(  // Chamada assíncrona
      params => {
        if (params.id) { // Se existir um parâmetro chamado id
          // 1) Vamos buscar os dados do objeto no back-end
          this.usersSrv.obterUm(params.id).subscribe(
            obj => { // Callback se OK
              this.users = obj;
              console.log(obj);
              this.titulo = 'Editar users';
            },
            erro => console.error(erro) // Callback se erro
          );
        }
      }
    );


  }

  salvar() {
    let retorno: any;
    if (this.users._id) {
      retorno = this.usersSrv.atualizar(this.users);
    } else {
      retorno = this.usersSrv.novo(this.users);
    }
    retorno.subscribe(
      () => {
        this.snackBar.open('Usuario salvo com sucesso', 'OK', { duration: 2000 });
        this.router.navigate(['users']); // Volta à listagem
      },
      erro => {
        this.snackBar.open('Erro ao salvar o users: ' + erro.message, 'OK');
        console.error(erro);
      }
    );
  }

  cancelar() {
    if (confirm('Deseja realmente cancelar as alterações?')) {
      this.router.navigate(['users']);
    }
  }


}
