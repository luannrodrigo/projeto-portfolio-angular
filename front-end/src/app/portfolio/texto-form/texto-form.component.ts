import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TextoService } from '../../services/texto.service';
import { UsersService } from '../../services/users.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-texto-form',
  templateUrl: './texto-form.component.html',
  styleUrls: ['./texto-form.component.css']
})
export class TextoFormComponent implements OnInit {

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private textoSrv: TextoService,
    private usersSrv: UsersService, 
    private snackBar: MatSnackBar
  ) { }

  public titulo: String = 'Novo Texto';

  public texto: any = {conjunto: false};
  public usuarios: any = [];


  ngOnInit() {

    // Pedimos para que o ActivatedRoute busque para nós os parâmetros da url
    this.actRoute.params.subscribe(  // Chamada assíncrona
      params => {
        if (params.id) { // Se existir um parâmetro chamado id
          // 1) Vamos buscar os dados do objeto no back-end
          this.textoSrv.obterUm(params.id).subscribe(
            obj => { // Callback se OK
              this.texto = obj;
              console.log(obj);
              this.titulo = 'Editar texto';
            },
            erro => console.error(erro) // Callback se erro
          );
        }
      }
    );

    this.usersSrv.listar().subscribe(
      dados => {
        this.usuarios = dados;
        console.log(dados);
      },
      erro => console.error(erro)
    );
  }

  salvar() {
    let retorno: any;
    if (this.texto._id) {
      retorno = this.textoSrv.atualizar(this.texto);
    } else {
      retorno = this.textoSrv.novo(this.texto);
    }
    retorno.subscribe(
      () => {
        this.snackBar.open('Texto salvo com sucesso', 'OK', { duration: 2000 });
        this.router.navigate(['texto']); // Volta à listagem
      },
      erro => {
        this.snackBar.open('Erro ao salvar o texto: ' + erro.message, 'OK');
        console.error(erro);
      }
    );
  }

  cancelar() {
    if (confirm('Deseja realmente cancelar as alterações?')) {
      this.router.navigate(['texto']);
    }
  }


}
