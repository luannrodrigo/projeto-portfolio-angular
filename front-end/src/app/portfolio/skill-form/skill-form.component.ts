import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SkillService } from '../../services/skill.service';
import { UsersService } from '../../services/users.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-skill-form',
  templateUrl: './skill-form.component.html',
  styleUrls: ['./skill-form.component.css']
})
export class SkillFormComponent implements OnInit {

  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private skillSrv: SkillService,
    private usersSrv: UsersService, 
    private snackBar: MatSnackBar
  ) { }

  public titulo: String = 'Nova habilidade';

  public skill: any = {conjunto: false};
  public usuarios: any = [];


  ngOnInit() {

    // Pedimos para que o ActivatedRoute busque para nós os parâmetros da url
    this.actRoute.params.subscribe(  // Chamada assíncrona
      params => {
        if (params.id) { // Se existir um parâmetro chamado id
          // 1) Vamos buscar os dados do objeto no back-end
          this.skillSrv.obterUm(params.id).subscribe(
            obj => { // Callback se OK
              this.skill = obj;
              console.log(obj);
              this.titulo = 'Editar skill';
            },
            erro => console.error(erro) // Callback se erro
          );
        }
      }
    );

    // this.skillSrv.listar().subscribe(
    //   dados => {
    //     this.skill = dados;
    //     console.log(dados);
    //   },
    //   erro => console.error(erro)
    // );


    this.usersSrv.listar().subscribe(
      dados => {
        this.usuarios = dados;
        console.log(dados);
      },
      erro => console.error(erro)
    );
  }

  salvar() {
    let retorno: any;
    if (this.skill._id) {
      retorno = this.skillSrv.atualizar(this.skill);
    } else {
      retorno = this.skillSrv.novo(this.skill);
    }
    retorno.subscribe(
      () => {
        this.snackBar.open('Skill salvo com sucesso', 'OK', { duration: 2000 });
        this.router.navigate(['skill']); // Volta à listagem
      },
      erro => {
        this.snackBar.open('Erro ao salvar o skill: ' + erro.message, 'OK');
        console.error(erro);
      }
    );
  }

  cancelar() {
    if (confirm('Deseja realmente cancelar as alterações?')) {
      this.router.navigate(['skill']);
    }
  }


}
