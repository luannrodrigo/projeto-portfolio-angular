import { Component, OnInit } from '@angular/core';
import { SkillService } from '../../services/skill.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-skill-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.css']
})
export class SkillListComponent implements OnInit {

  public skills: any;

  public colunasVisiveis: string[] = [
    'nome_skill',
    'desc_skill',
    'usuario', 
    'excluir'
  ];

  // Injeção de dependência no parâmetro do construtor
  constructor(
    private skillSrv: SkillService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    // Chamando o service
    this.skillSrv.listar().subscribe(
       dados => this.skills = dados, // Callback do bem
       erro => console.error(erro) // Callback do mal
    );
  }

  excluir(id: String) {
    if (confirm('Deseja realmente excluir este artigo?')) {
      this.skillSrv.excluir(id).subscribe(
        () => {
          this.snackBar.open('Artigo excluído com sucesso', 'OK', { duration: 2000});
          this.ngOnInit(); // Recarrega a lista
        },
        erro => this.snackBar.open('ERRO AO EXCLUIR ARTIGO: ' + erro.message, 'OK')
      );
    }
  }

}
