import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextoListComponent } from './texto-list.component';

describe('TextoListComponent', () => {
  let component: TextoListComponent;
  let fixture: ComponentFixture<TextoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
