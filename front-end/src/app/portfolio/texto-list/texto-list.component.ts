import { Component, OnInit } from '@angular/core';
import { TextoService } from '../../services/texto.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-texto-list',
  templateUrl: './texto-list.component.html',
  styleUrls: ['./texto-list.component.css']
})
export class TextoListComponent implements OnInit {

  public textos: any;

  public colunasVisiveis: string[] = [
    'titulo',
    'texto',
    'usuario', 
    'excluir'
  ];

  // Injeção de dependência no parâmetro do construtor
  constructor(
    private textoSrv: TextoService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    // Chamando o service
    this.textoSrv.listar().subscribe(
       dados => this.textos = dados, // Callback do bem
       erro => console.error(erro) // Callback do mal
    );
  }

  excluir(id: String) {
    if (confirm('Deseja realmente excluir este artigo?')) {
      this.textoSrv.excluir(id).subscribe(
        () => {
          this.snackBar.open('Artigo excluído com sucesso', 'OK', { duration: 2000});
          this.ngOnInit(); // Recarrega a lista
        },
        erro => this.snackBar.open('ERRO AO EXCLUIR ARTIGO: ' + erro.message, 'OK')
      );
    }
  }

}
